var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
	    .when('/', {
	      controller: 'BookingController',
	      templateUrl: 'views/book/create.html'
	    })
	    .otherwise({
	      redirectTo:'/'
	    });
	});