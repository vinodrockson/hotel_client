package hotel.client.service;

import hotel.client.repositories.HotelRepository;
import hotel.client.resources.BookingResource;
import hotel.client.resources.HotelResource;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class RentalService {
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	HotelRepository hRepo;
	
	@Value("${hotelserver.host:}")
	String host;

	//Gets the list of available hotels from the server
	public List<HotelResource> findAvailableHotels(String hotelName, int persons,
			Date startDate, Date endDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		HotelResource[] hotels = restTemplate
				.getForObject(
						host+"/rest/hotels/query?name={name}&startDate={start}&endDate={end}",
						HotelResource[].class, hotelName,
						formatter.format(startDate), formatter.format(endDate));

		return Arrays.asList(hotels);
	}
	
	//Send the booking made from client to server
	public BookingResource sendBooking(BookingResource supRes) throws RestClientException, JsonProcessingException {

		ResponseEntity<BookingResource> supR = restTemplate.postForEntity(
				host+"/rest/booking", supRes, BookingResource.class);
		return supR.getBody();
	}
	

 }
