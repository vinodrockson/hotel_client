package hotel.client.assembler;

import hotel.client.controllers.BookingController;
import hotel.client.models.Booking;
import hotel.client.resources.BookingResource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

public class BookingAssembler extends ResourceAssemblerSupport<Booking, BookingResource> {
	
	public BookingAssembler() {
		super(BookingController.class, BookingResource.class);
		}
	
	@Override
	public BookingResource toResource(Booking book) {
		BookingResource bookResource = createResourceWithId(
			book.getId(), book);
		bookResource.setClient(book.getClient());
		bookResource.setHotelid(book.getHotelid());
		bookResource.setStartDate(book.getStartDate());
		bookResource.setEndDate(book.getEndDate());
		bookResource.setPersons(book.getPersons());
		return bookResource;	
	}

	public List<BookingResource> toResource(List<Booking> bookList) {
	List<BookingResource> bookRes = new ArrayList<>();
	
	for (Booking sup : bookList) {
		bookRes.add(toResource(sup));
	}
	
	return bookRes;
	}
}

