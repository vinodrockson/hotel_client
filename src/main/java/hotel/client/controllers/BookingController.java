package hotel.client.controllers;

import hotel.client.assembler.BookingAssembler;
import hotel.client.exception.BookingNotFoundException;
import hotel.client.models.Booking;
import hotel.client.repositories.BookingRepository;
import hotel.client.resources.BookingResource;
import hotel.client.service.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@RequestMapping("/bookings")
public class BookingController {
	@Autowired
    RentalService rentProxy;
	
	@Autowired
	BookingRepository bRepo;
	
	//Creates the booking
	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<BookingResource> createBooking(
			@RequestBody BookingResource res) throws RestClientException, BookingNotFoundException, JsonProcessingException{

		ResponseEntity<BookingResource> resp;
		
		if (res == null) {
			throw new BookingNotFoundException(null);
		}
		
		Booking book = new Booking();
		book.setClient(res.getClient());
		book.setStartDate(res.getStartDate());
		book.setEndDate(res.getEndDate());
		book.setPersons(res.getPersons());
		book.setHotelid(res.getHotelid());
		
		book = bRepo.saveAndFlush(book);
		
		BookingAssembler bassem = new BookingAssembler();
		BookingResource newRes = bassem.toResource(book);		
		BookingResource bookReso= rentProxy.sendBooking(newRes);
	
		resp = new ResponseEntity<BookingResource>(
				bookReso, HttpStatus.OK);
		return resp;
	}
	
	@ExceptionHandler(BookingNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handPONotFoundException(BookingNotFoundException ex) {
		
	}

}
