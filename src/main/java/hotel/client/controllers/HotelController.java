package hotel.client.controllers;


import hotel.client.resources.HotelResource;
import hotel.client.service.RentalService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/hotels")
public class HotelController {
	@Autowired
    RentalService rentProxy;
	
	//Gets the list of hotels from the server
	@RequestMapping(method=RequestMethod.GET, value="/query")
	@ResponseStatus(HttpStatus.OK)
	public List<HotelResource> listHotelCatalog(@RequestParam("hotel") String name, @RequestParam("persons") int persons,
			@RequestParam("startDate") String date1, @RequestParam("endDate") String date2) throws Exception{
		
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		    Date Date1 = df.parse(date1);
		    Date Date2 = df.parse(date2);
		    return rentProxy.findAvailableHotels(name, persons, Date1, Date2);
	}

}