package hotel.client.exception;

public class BookingNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public BookingNotFoundException(Long id) {
		super(String.format("Hotel Not found! (Hotel id: %d)", id));
	}
}
