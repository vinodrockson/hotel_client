var app = angular.module('project');

app.controller('BookingController', function($scope, $http, $location) {
	$scope.hotels = [];
	$scope.hotel = '';
	$scope.persons = 0;
	$scope.startDate = new Date();
	$scope.endDate = new Date();
	
	
	console.log($location.path() == "/");
	$scope.catalogShown = false;
	
	$scope.execQuery = function () {
	        $http.get('/rest/hotels/query', {params: {hotel: $scope.hotel, persons: $scope.persons, startDate: $scope.startDate,
	        	endDate: $scope.endDate}}).success(function(data, status, headers, config) {
	        		if ((data || []).length === 0) {
	        		    alert('No rooms available with your current selection. Try with another selection')
	        		}
	        		else{
			            $scope.hotels = data;
			            console.log(JSON.stringify(data));
			            $scope.catalogShown = true;
	        		}
	        });
    };
	
 
    $scope.setHotel = function (selectedId, client) {
	    booking = { hotelid: selectedId, persons: $scope.persons, startDate: $scope.startDate, endDate: $scope.endDate, client: client};
	        $http.post('/bookings', booking).success(function(data, status, headers, config) {
	            
	        	if(!alert('Your booking has been successfully made')){window.location.reload();}
	        });

	};
});

